<?php

namespace App;

interface ComplexNumberInterface
{
    /**
     * Умножение
     * @return mixed
     */
    public function multiply(ComplexNumber $leftOperand, ComplexNumber $rightOperand);

    /**
     * Деление
     * @return mixed
     */
    public function divide(ComplexNumber $leftOperand, ComplexNumber $rightOperand);

    /**
     * Сложить
     * @return mixed
     */
    public function fold(ComplexNumber $leftOperand, ComplexNumber $rightOperand);

    /**
     * Вычесть
     * @return mixed
     */
    public function subtract(ComplexNumber $leftOperand, ComplexNumber $rightOperand);
}