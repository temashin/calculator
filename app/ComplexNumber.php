<?php

namespace App;

class ComplexNumber
{

    /**
     * Действительная часть комплексного числа
     * @var float
     */
    private $real;

    /**
     * Мнимая часть комплексного числа
     * @var float
     */
    private $imaginary;


    /**
     * @return float
     */
    public function getImaginary()
    {
        return $this->imaginary;
    }

    /**
     * @return float
     */
    public function getReal()
    {
        return $this->real;
    }

    /**
     * @param float $real
     */
    public function setReal($real): void
    {
        $this->real = $real;
    }

    /**
     * @param float $imaginary
     */
    public function setImaginary($imaginary): void
    {
        $this->imaginary = $imaginary;
    }

    public function __construct($real, $imaginary)
    {
        $this->real = $real;
        $this->imaginary = $imaginary;
    }

    public function multiply(ComplexNumber $leftOperand, ComplexNumber $rightOperand)
    {
        return new ComplexNumber(
            floatval(
                bcsub(
                    bcmul((string)$leftOperand->getReal(), (string)$rightOperand->getReal()),
                    bcmul((string)$leftOperand->getImaginary(), (string)$rightOperand->getImaginary())
                )
            ),
            floatval(
                bcadd(
                    bcmul((string)$leftOperand->getReal(), (string)$rightOperand->getImaginary()),
                    bcmul((string)$rightOperand->getReal(), (string)$leftOperand->getImaginary())
                )
            )
        );
    }

    public function divide(ComplexNumber $dividend, ComplexNumber $divisor)
    {
        return new ComplexNumber(
            floatval(
                bcdiv(
                    bcadd(
                        bcmul((string)$dividend->getReal(), (string)$divisor->getReal()),
                        bcmul((string)$dividend->getImaginary(), (string)$divisor->getImaginary())
                    ),
                    bcadd(
                        bcpow((string)$divisor->getReal(), '2'),
                        bcpow((string)$divisor->getImaginary(), '2')
                    )
                )
            ),
            floatval(
                bcdiv(
                    bcsub(
                        bcmul((string)$dividend->getImaginary(), (string)$divisor->getReal()),
                        bcmul((string)$dividend->getReal(), (string)$divisor->getImaginary())
                    ),
                    bcadd(
                        bcpow((string)$divisor->getRealValue(), '2'),
                        bcpow((string)$divisor->getImaginaryValue(), '2')
                    )
                )
            )
        );
    }

    public function fold(ComplexNumber $leftOperand, ComplexNumber $rightOperand)
    {
        return new ComplexNumber(
            floatval(
                bcadd(
                    (string)$leftOperand->getReal(),
                    (string)$rightOperand->getReal()
                )
            ),
            floatval(
                bcadd(
                    (string)$leftOperand->getImaginary(),
                    (string)$rightOperand->getImaginary()
                )
            )
        );
    }

    public function subtract(ComplexNumber $leftOperand, ComplexNumber $rightOperand)
    {
        return new ComplexNumber(
            floatval(
                bcsub(
                    (string)$leftOperand->getReal(),
                    (string)$rightOperand->getReal()
                )
            ),
            floatval(
                bcsub(
                    (string)$leftOperand->getImaginary(),
                    (string)$rightOperand->getImaginary()
                )
            )
        );
    }
}