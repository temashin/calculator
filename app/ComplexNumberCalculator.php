<?php

namespace App;

use Exception;

class ComplexNumberCalculator implements ComplexNumberInterface
{

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @return ComplexNumber
     */
    public function multiply(ComplexNumber $left, ComplexNumber $right): ComplexNumber
    {
        return new ComplexNumber(
            floatval(
                bcsub(
                    bcmul((string)$left->getReal(), (string)$right->getReal()),
                    bcmul((string)$left->getImaginary(), (string)$right->getImaginary())
                )
            ),
            floatval(
                bcadd(
                    bcmul((string)$left->getReal(), (string)$right->getImaginary()),
                    bcmul((string)$right->getReal(), (string)$left->getImaginary())
                )
            )
        );
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @return ComplexNumber
     * @throws Exception
     */
    public function divide(ComplexNumber $left, ComplexNumber $right): ComplexNumber
    {
        //на 0 делить не можем
        if (floatval(0) === $right->getReal() && floatval(0) === $right->getImaginary()){
            throw new Exception('Ooops делить на 0 нельзя',);
        }
        
        return new ComplexNumber(
            floatval(
                bcdiv(
                    bcadd(
                        bcmul((string)$left->getReal(), (string)$right->getReal()),
                        bcmul((string)$left->getImaginary(), (string)$right->getImaginary())
                    ),
                    bcadd(
                        bcpow((string)$right->getReal(), '2'),
                        bcpow((string)$right->getImaginary(), '2')
                    )
                )
            ),
            floatval(
                bcdiv(
                    bcsub(
                        bcmul((string)$left->getImaginary(), (string)$right->getReal()),
                        bcmul((string)$left->getReal(), (string)$right->getImaginary())
                    ),
                    bcadd(
                        bcpow((string)$right->getReal(), '2'),
                        bcpow((string)$right->getImaginary(), '2')
                    )
                )
            )
        );
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @return ComplexNumber
     */
    public function fold(ComplexNumber $left, ComplexNumber $right): ComplexNumber
    {
        return new ComplexNumber(
            floatval(
                bcadd(
                    (string)$left->getReal(),
                    (string)$right->getReal()
                )
            ),
            floatval(
                bcadd(
                    (string)$left->getImaginary(),
                    (string)$right->getImaginary()
                )
            )
        );
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @return ComplexNumber
     */
    public function subtract(ComplexNumber $left, ComplexNumber $right): ComplexNumber
    {
        return new ComplexNumber(
            floatval(
                bcsub(
                    (string)$left->getReal(),
                    (string)$right->getReal()
                )
            ),
            floatval(
                bcsub(
                    (string)$left->getImaginary(),
                    (string)$right->getImaginary()
                )
            )
        );
    }
}