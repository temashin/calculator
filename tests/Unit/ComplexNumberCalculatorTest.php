<?php

use App\ComplexNumber;
use App\ComplexNumberCalculator;
use PHPUnit\Framework\TestCase;

class ComplexNumberCalculatorTest extends TestCase
{
    /**
     * @return \App\ComplexNumber[][]
     */
    public function foldProvider()
    {
        return [
            [new ComplexNumber(0, 1), new ComplexNumber(-1.12, 5.05), new ComplexNumber(1, 1)],
            [new ComplexNumber(0.23, 5.5), new ComplexNumber(-1.12, 5.05), new ComplexNumber(10, 11)],
        ];
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @param $expectedResult
     * @dataProvider foldProvider
     */
    public function testFold(ComplexNumber $left, ComplexNumber $right , ComplexNumber $expectedResult)
    {
        $calculator = new ComplexNumberCalculator();
        $resultFold = $calculator->fold($left, $right);
        $this->assertFalse($this->compare($expectedResult, $resultFold));
    }

    /**
     * @return \App\ComplexNumber[][]
     */
    public function divideProvider()
    {
        return [
            [new ComplexNumber(0, 1), new ComplexNumber(-1.12, 5.05), new ComplexNumber(1, 1)],
            [new ComplexNumber(0.23, 5.5), new ComplexNumber(-1.12, 5.05), new ComplexNumber(10, 11)],
        ];
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @param ComplexNumber $expectedResult
     * @dataProvider divideProvider
     */
    public function testDivide(ComplexNumber $left, ComplexNumber $right, ComplexNumber $expectedResult)
    {
        $calculator = new ComplexNumberCalculator();
        $resultFold = $calculator->divide($left, $right);
        $this->assertFalse($this->compare($expectedResult, $resultFold));
    }
    
    /**
     * @return \App\ComplexNumber[][]
     */
    public function subtractProvider()
    {
        return [
            [new ComplexNumber(0, 1), new ComplexNumber(-1.12, 5.05), new ComplexNumber(1, 1)],
            [new ComplexNumber(0.23, 5.5), new ComplexNumber(-1.12, 5.05), new ComplexNumber(10, 11)],
        ];
    }

    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @param ComplexNumber $expectedResult
     * @dataProvider subtractProvider
     */
    public function testSubtract(ComplexNumber $left, ComplexNumber $right, ComplexNumber $expectedResult)
    {
        $calculator = new ComplexNumberCalculator();
        $resultFold = $calculator->subtract($left, $right);
        $this->assertFalse($this->compare($expectedResult, $resultFold));
    }

    /**
     * @return \App\ComplexNumber[][]
     */
    public function multiplyProvider()
    {
        return [
            [new ComplexNumber(0, 1), new ComplexNumber(-1.12, 5.05), new ComplexNumber(1, 1)],
            [new ComplexNumber(0.23, 5.5), new ComplexNumber(-1.12, 5.05), new ComplexNumber(10, 11)],
        ];
    }
    
    /**
     * @param ComplexNumber $left
     * @param ComplexNumber $right
     * @param ComplexNumber $expectedResult
     * @dataProvider multiplyProvider
     */
    public function testMultiply(ComplexNumber $left, ComplexNumber $right, ComplexNumber $expectedResult)
    {
        $calculator = new ComplexNumberCalculator();
        $resultFold = $calculator->multiply($left, $right);
        $this->assertFalse($this->compare($expectedResult, $resultFold));
    }

    /**
     * @param ComplexNumber $a
     * @param ComplexNumber $b
     * @return bool
     */
    public function compare(ComplexNumber $a, ComplexNumber $b)
    {
        if(
            abs($a->getReal() - $b->getReal()) < PHP_FLOAT_EPSILON &&
            abs($a->getImaginary() - $b->getImaginary()) < PHP_FLOAT_EPSILON
        ){
            return true;
        }else
            return false;
    }

}