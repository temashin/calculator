<?php

use App\ComplexNumber;
use App\ComplexNumberCalculator;

require 'vendor/autoload.php';


$a = new ComplexNumber(1, 5.5);
$b = new ComplexNumber(1, -7.19);

$calculator = new ComplexNumberCalculator();

$result = $calculator->fold($a, $b);

echo sprintf('%s + %si', $result->getReal(), $result->getImaginary());
